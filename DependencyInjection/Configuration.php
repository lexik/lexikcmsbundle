<?php

namespace Lexik\Bundle\CMSBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('lexik_cms');

        $rootNode
            ->children()
                ->scalarNode('frontend_layout')
                    ->defaultValue('::frontend.html.twig')
                    ->cannotBeEmpty()
                ->end()
                ->booleanNode('crud_integration')
                    ->defaultTrue()
                ->end()
                ->booleanNode('edit_in_place')
                    ->defaultTrue()
                ->end()
                ->booleanNode('sonata_integration')
                    ->defaultFalse()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
