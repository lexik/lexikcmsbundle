var editorConfig = {
    on: {
        focus: function() {
            $(this.container.$).closest('[data-editable-type]').addClass('active');
        },
        blur: function() {
            $(this.container.$).closest('[data-editable-type]').removeClass('active');
        }
    },
    filebrowserBrowseUrl: elFinderUrl,
    extraPlugins: 'submit',
    toolbarGroups: [
        { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
        { name: 'editing',     groups: [ 'find', 'selection' ] },
        { name: 'links' },
        { name: 'insert' },
        { name: 'custom' },
        { name: 'tools' },
        { name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
        '/',
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
        { name: 'styles' },
    ]
};

var submitContainer = function(container) {
    var container = $(container).closest('div[data-editable-type]');

    $.each(CKEDITOR.instances, function() {
        this.destroy();
    });

    $(container).find('.editable').each(function() {
        $(this).removeAttr('contenteditable').removeClass('cke_focus');
    });

    $(container).removeClass('active');

    $.ajax({
        url: $(container).data('url'),
        data: {
            content: $(container).html()
        },
        type: 'PUT',
        success: function() {
            $('div[data-editable-type] .editable').each(function() {
                $(this).attr('contenteditable', 'true');
                CKEDITOR.inline(this, editorConfig);
            });
        }
    });
};

$(function() {
    CKEDITOR.disableAutoInline = true;
    CKEDITOR.plugins.addExternal('submit', '/bundles/lexikcms/ckeditor/plugins/submit/plugin.js');

    $('div[data-editable-type] .editable').each(function() {
        $(this).attr('contenteditable', 'true');
        CKEDITOR.inline(this, editorConfig);
    });
});
