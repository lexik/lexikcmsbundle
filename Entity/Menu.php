<?php

namespace Lexik\Bundle\CMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Lexik\Bundle\CMSBundle\Entity\Menu
 *
 * @ORM\Table(name="lexik_cms_menu")
 * @ORM\Entity(repositoryClass="Lexik\Bundle\CMSBundle\Repository\MenuRepository")
 */
class Menu
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255)
     *
     * @Assert\NotBlank()
     */
    private $label;

    /**
     * @var Page
     *
     * @ORM\ManyToOne(targetEntity="Lexik\Bundle\CMSBundle\Entity\Page", inversedBy="menus")
     */
    private $page;

    /**
     * @var string
     *
     * @ORM\Column(name="route", type="string", length=255, nullable=true)
     */
    private $route;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="unit", type="string", length=64)
     *
     * @Assert\NotBlank()
     */
    private $unit;

    /**
     * @var integer
     *
     * @ORM\Column(name="sequence", type="integer")
     *
     * @Assert\NotBlank()
     */
    private $sequence;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->unit     = 'main';
        $this->sequence = 1;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getLabel();
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $label
     */
    public function setlabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getlabel()
    {
        return $this->label;
    }

    /**
     * @param Page $page
     */
    public function setPage(Page $page)
    {
        $this->page = $page;
    }

    /**
     * @return Page
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param string $route
     */
    public function setRoute($route)
    {
        $this->route = $route;
    }

    /**
     * @return string
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $unit
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
    }

    /**
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param int $sequence
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
    }

    /**
     * @return string
     */
    public function getSequence()
    {
        return $this->sequence;
    }
}
