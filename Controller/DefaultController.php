<?php

namespace Lexik\Bundle\CMSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class DefaultController
 * @package Lexik\Bundle\CMSBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * Route("/", name="homepage")
     */
    public function homepageAction()
    {
        $homepage = $this
            ->getDoctrine()
            ->getRepository('LexikCMSBundle:Page')
            ->getCachedHomepage();

        if (null === $homepage) {
            throw $this->createNotFoundException('Unable to find any homepage...');
        }

        return $this->render('LexikCMSBundle:Default:page.html.twig', array(
            'page' => $homepage,
        ));
    }

    /**
     * @Route("/content/{name}", name="block_content", requirements={"slug" = ".*"})
     */
    public function contentAction($name)
    {
        $block = $this
            ->getDoctrine()
            ->getRepository('LexikCMSBundle:Block')
            ->findOneBy(array(
                    'name' => $name,
                ))
        ;

        if (null === $block) {
            throw $this->createNotFoundException('Content not found.');
        }

        return $this->render('LexikCMSBundle:Default:_block.html.twig', array(
            'content' => $block->getContent(),
        ));
    }

    /**
     * @Route("/{slug}", name="page", requirements={"slug" = ".*"})
     */
    public function pageAction($slug)
    {
        $page = $this
            ->getDoctrine()
            ->getRepository('LexikCMSBundle:Page')
            ->getCachedPage($slug)
        ;

        if (null === $page) {
            throw $this->createNotFoundException(sprintf('Unable to find page with slug "%s".', $slug));
        }

        return $this->render('LexikCMSBundle:Default:page.html.twig', array(
            'page'             => $page,
            'frontend_laytout' => $this->container->getParameter('lexik_cms.frontend_layout'),
        ));
    }
}
