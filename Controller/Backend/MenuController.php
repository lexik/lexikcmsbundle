<?php

namespace Lexik\Bundle\CMSBundle\Controller\Backend;

use Lexik\Bundle\CrudBundle\Controller\CrudController;
use Lexik\Bundle\CrudBundle\Routing\Route;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * @Route("/menu", name="crud_cms_menu", serviceId="lexik_cms.crud.menu")
 */
class MenuController extends CrudController
{
    /**
     * {@inheritdoc}
     */
    protected function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'form_class_name' => 'Lexik\Bundle\CMSBundle\Form\Type\MenuType',
            'templates'       => array(
                'new'  => 'LexikCMSBundle:Backend/Menu:form.html.twig',
                'edit' => 'LexikCMSBundle:Backend/Menu:form.html.twig',
            ),
        ));
    }
}
