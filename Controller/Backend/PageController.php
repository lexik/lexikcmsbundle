<?php

namespace Lexik\Bundle\CMSBundle\Controller\Backend;

use Lexik\Bundle\CrudBundle\Controller\CrudController;
use Lexik\Bundle\CrudBundle\Routing\Route;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * @Route("/page", name="crud_cms_page", serviceId="lexik_cms.crud.page")
 */
class PageController extends CrudController
{
    /**
     * {@inheritdoc}
     */
    protected function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'form_class_name' => 'Lexik\Bundle\CMSBundle\Form\Type\PageType',
            'templates'       => array(
                'new'  => 'LexikCMSBundle:Backend/Page:form.html.twig',
                'edit' => 'LexikCMSBundle:Backend/Page:form.html.twig',
            ),
        ));
    }
}
