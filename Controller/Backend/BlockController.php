<?php

namespace Lexik\Bundle\CMSBundle\Controller\Backend;

use Lexik\Bundle\CrudBundle\Controller\CrudController;
use Lexik\Bundle\CrudBundle\Routing\Route;

use Symfony\Component\Form\Form;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * @Route("/block", name="crud_cms_block", serviceId="lexik_cms.crud.block")
 */
class BlockController extends CrudController
{
    /**
     * {@inheritdoc}
     */
    protected function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'form_class_name'   => 'Lexik\Bundle\CMSBundle\Form\Type\BlockType',
            'filter_class_name' => 'Lexik\Bundle\CMSBundle\Form\Type\Filter\BlockType',
        ));
    }

    /**
     * @param mixed $object
     * @param Form $form
     */
    protected function update($object, Form $form = null)
    {
        parent::update($object, $form);

        $this->container->get('twig')->clearCacheFiles();
    }
}
