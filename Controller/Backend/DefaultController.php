<?php

namespace Lexik\Bundle\CMSBundle\Controller\Backend;

use Lexik\Bundle\CMSBundle\Entity\Block;
use Lexik\Bundle\CMSBundle\Entity\Page;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * DefaultController
 */
class DefaultController extends Controller
{
    /**
     * @param integer $id
     * @param Request $request
     *
     * @return Response
     * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
     *
     * @Route("/page/{id}/ajax-update", name="cms_ajax_page_update", requirements={"method" = "PUT"})
     */
    public function ajaxPageUpdateAction($id, Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $page = $em->find('LexikCMSBundle:Page', $id);

        if (!$page instanceof Page) {
            throw new AccessDeniedHttpException();
        }

        $content = $request->get('content', null);
        $page->setContent($content);
        $em->flush();

        return new Response();
    }

    /**
     * @param integer $id
     * @param Request $request
     *
     * @return Response
     * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
     *
     * @Route("/block/{id}/ajax-update", name="cms_ajax_block_update", requirements={"method" = "PUT"})
     */
    public function ajaxUpdateBlockAction($id, Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $block = $em->find('LexikCMSBundle:Block', $id);

        if (!$block instanceof Block) {
            throw new AccessDeniedHttpException();
        }

        $content = $request->get('content', null);
        $block->setContent($content);
        $em->flush();

        return new Response();
    }

    /**
     * Renders Elfinder
     * @return Response
     *
     * @Route("/el_finder", name="el_finder")
     */
    public function elFinderAction()
    {
        $parameters = $this->container->getParameter('fm_elfinder');
        $editor = $this->getRequest()->get('editor', $parameters['editor']);
        $locale = $parameters['locale'] ?: $this->getRequest()->getLocale();
        $fullscreen = $parameters['fullscreen'];
        $includeAssets = $parameters['include_assets'];
        $compression = $parameters['compression'];
        $prefix = ($compression ? '/compressed' : '');

        switch ($editor) {
            case 'ckeditor':
                return $this->render('FMElfinderBundle:Elfinder'.$prefix.':ckeditor.html.twig', array(
                        'locale' => $locale,
                        'fullscreen' => $fullscreen,
                        'includeAssets' => $includeAssets
                    ));
                break;
            default:
                return $this->render('FMElfinderBundle:Elfinder'.$prefix.':simple.html.twig', array(
                        'locale' => $locale,
                        'fullscreen' => $fullscreen,
                        'includeAssets' => $includeAssets,
                    ));
        }
    }
}
