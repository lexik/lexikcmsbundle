<?php

namespace Lexik\Bundle\CMSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class MenuController
 * @package Lexik\Bundle\CMSBundle\Controller
 */
class MenuController extends Controller
{
    /**
     * Action used to build the menu in the layout.
     *
     * @param string $unit
     * @param string $active
     *
     * @return Response
     */
    public function showAction($unit, $active)
    {
        return $this->render('LexikCMSBundle:Menu:menu.html.twig', array(
            'menu_unit' => $unit,
            'menus'     => $this->getDoctrine()->getRepository('Lexik\Bundle\CMSBundle\Entity\Menu')->getCachedMenuByUnitIndexedById($unit),
            'active'    => $active,
        ));
    }
}
