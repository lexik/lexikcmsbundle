<?php

namespace Lexik\Bundle\CMSBundle\Listener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\UnitOfWork;
use Lexik\Bundle\CMSBundle\Entity\Block;
use Lexik\Bundle\CMSBundle\Entity\Menu;
use Lexik\Bundle\CMSBundle\Entity\Page;
use Lexik\Bundle\CMSBundle\Service\PageTransformer;

/**
 * CMSSubscriber
 */
class CMSSubscriber implements EventSubscriber
{
    /**
     * @var PageTransformer
     */
    private $pageTransformer;

    /**
     * @param PageTransformer $pageTransformer
     */
    public function __construct(PageTransformer $pageTransformer)
    {
        $this->pageTransformer = $pageTransformer;
    }

    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
        return array(
            Events::prePersist,
            Events::preUpdate,
            Events::preRemove,
        );
    }

    /**
     * {@inheritdoc}
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $this->clearCache($args);
        $this->pageTransformation($args);

        $entity = $args->getEntity();
        if ($entity instanceof Block) {
            $entity->setLastModified(new \DateTime());
        }
    }

    /**
     * {@inheritdoc}
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        $this->clearCache($args);
        $this->pageTransformation($args);

        $entity = $args->getEntity();
        if ($entity instanceof Block) {
            $entity->setLastModified(new \DateTime());
        }
    }

    /**
     * {@inheritdoc}
     */
    public function preRemove(LifecycleEventArgs $args)
    {
        $this->clearCache($args);
    }

    /**
     * Transforms a page.
     *
     * @param LifecycleEventArgs $args
     */
    public function pageTransformation(LifecycleEventArgs $args)
    {
        $em = $args->getEntityManager();
        $entity = $args->getEntity();

        if ($entity instanceof Page) {
            $this->pageTransformer->transform($entity, $em);

            if (UnitOfWork::STATE_MANAGED === $em->getUnitOfWork()->getEntityState($entity)) {
                $em->getUnitOfWork()->recomputeSingleEntityChangeSet(
                    $em->getClassMetadata('LexikCMSBundle:Page'),
                    $entity
                );
            }
        }
    }

    /**
     * Clear cached data for given entity
     *
     * @param LifecycleEventArgs $args
     */
    public function clearCache(LifecycleEventArgs $args)
    {
        $em = $args->getEntityManager();
        $entity = $args->getEntity();

        if ($entity instanceof Page) {
            $em->getConfiguration()->getResultCacheImpl()->delete('pages');
            $em->getConfiguration()->getResultCacheImpl()->delete('homepage');
            $em->getConfiguration()->getResultCacheImpl()->delete(sprintf('page_%s', $entity->getSlug()));
        }

        if ($entity instanceof Menu) {
            $em->getConfiguration()->getResultCacheImpl()->delete('menus');
            $em->getConfiguration()->getResultCacheImpl()->delete(sprintf('menu_%s', $entity->getUnit()));
        }

        if ($entity instanceof Block) {
            $em->getConfiguration()->getResultCacheImpl()->delete('blocks');
        }
    }
}
