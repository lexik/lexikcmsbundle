<?php

namespace Lexik\Bundle\CMSBundle\Twig;

use Doctrine\ORM\EntityManager;
use Lexik\Bundle\CMSBundle\Service\Router;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class DatabaseTwigLoader
 *
 * @author Gilles Gauthier <g.gauthier@lexik.fr>
 */
class DatabaseTwigLoader implements \Twig_LoaderInterface, \Twig_ExistsLoaderInterface
{
    /**
     * @var Router $router
     */
    protected $router;

    /**
     * @param RouterInterface $router
     * @param EntityManager   $em
     */
    public function __construct(RouterInterface $router, EntityManager $em)
    {
        $this->router = new Router($router, $em);
    }

    /**
     * {@inheritdoc}
     */
    public function getSource($slug)
    {
        if (false === $source = $this->getValue($slug)) {
            throw new \Twig_Error_Loader(sprintf('Template "%s" does not exist.', $slug));
        }

        return $source['content'];
    }

    /**
     * {@inheritdoc}
     */
    public function exists($name)
    {
        return $this->getValue($name) !== null;
    }

    /**
     * {@inheritdoc}
     */
    public function getCacheKey($name)
    {
        return $name;
    }

    /**
     * {@inheritdoc}
     */
    public function isFresh($slug, $time)
    {
        $source = $this->getValue($slug);
        if (false === $lastModified = $source['lastModified']->getTimestamp()) {
            return false;
        }

        return $lastModified <= $time;
    }

    /**
     * @param string $slug
     *
     * @return string
     */
    protected function getValue($slug)
    {
        return $this->router->getBlockContent($slug, false);
    }
} 
