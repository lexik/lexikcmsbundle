<?php

namespace Lexik\Bundle\CMSBundle\Twig;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\RouterInterface;
use Lexik\Bundle\CMSBundle\Service\Router;

/**
 * CMSExtension
 */
class CMSExtension extends \Twig_Extension
{
    /**
     * @var Router $router
     */
    private $router;

    /**
     * @var \Twig_Environment
     */
    private $environment;

    /**
     * @var boolean
     */
    private $editInPlace;

    /**
     * @param RouterInterface   $router
     * @param EntityManager     $em
     * @param \Twig_Environment $environment
     * @param boolean           $editInPlace
     */
    public function __construct(RouterInterface $router, EntityManager $em, \Twig_Environment $environment, $editInPlace)
    {
        $this->router = new Router($router, $em);
        $this->environment = $environment;
        $this->editInPlace = $editInPlace;
    }

    /**
     * {@inheritDoc}
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('page_path', array($this, 'getPagePath')),
            new \Twig_SimpleFunction('menu_path', array($this, 'getMenuPath')),
            new \Twig_SimpleFunction('page_content', array($this, 'getPageContent'), array(
                'is_safe' => array('html'),
            )),
            new \Twig_SimpleFunction('cms_block', array($this, 'getBlockContent'), array(
                'is_safe' => array('html'),
            )),
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getGlobals()
    {
        return array(
            'cms_edit_in_place' => $this->editInPlace,
        );
    }

    /**
     * @see Router::getPagePath()
     */
    public function getPagePath($id)
    {
        return $this->router->getPagePath($id);
    }

    /**
     * @see Router::getMenuPath()
     */
    public function getMenuPath($id)
    {
        return $this->router->getMenuPath($id);
    }

    /**
     * @see Router::getBlockContent()
     */
    public function getBlockContent($reference, $editable = true, $object = null, array $parameters = array())
    {
        $block = $this->router->getBlockContent($reference);

        if (null === $block) {
            return '';
        }

        return $this->environment->render('LexikCMSBundle:Default:block.html.twig', array_merge(array(
            'templateName' => $reference,
            'block'        => $block,
            'editable'     => $editable,
            'object'       => $object
        ), $parameters));
    }

    /**
     * @param string $slug
     *
     * @return string
     */
    public function getPageContent($slug)
    {
        $page = $this->router->getPageContent($slug);

        if (null !== $page) {
            return $page['transformedContent'];
        }

        return '';
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'lexik_cms_extension';
    }
}
