<?php

namespace Lexik\Bundle\CMSBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * PageAdmin
 *
 * @author Nicolas Cabot <n.cabot@lexik.fr>
 */
class PageAdmin extends Admin
{
    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('show');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('pageTitle')
            ->add(
                'lastModified',
                'date',
                [
                    'format' => 'yyyy-MM-dd',
                ]
            )
            ->add(
                '_action',
                'actions',
                [
                    'actions' => [
                        'edit'   => [],
                        'delete' => [],
                    ],
                ]
            )
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General')
                ->add('pageTitle')
                ->add('content', null, array('attr' => array('data-editor' => 'html')))
            ->end()
            ->with('SEO')
                ->add('metaTitle')
                ->add('metaDescription', 'textarea', array('required' => false))
                ->add('slug', null, array('required' => false))
            ->end()
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter->add('pageTitle');
    }
}
