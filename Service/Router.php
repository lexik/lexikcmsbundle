<?php

namespace Lexik\Bundle\CMSBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\RouterInterface;

/**
 * Router
 */
class Router
{
    /**
     * @var RouterInterface $router
     */
    private $router;

    /**
     * @var EntityManager $em
     */
    private $em;

    /**
     * @param RouterInterface $router
     * @param EntityManager   $em
     */
    public function __construct(RouterInterface $router, EntityManager $em)
    {
        $this->router = $router;
        $this->em     = $em;
    }

    /**
     * Generate path to a Page.
     *
     * @param integer $id
     *
     * @return string
     */
    public function getPagePath($id)
    {
        $pages = $this->em->getRepository('LexikCMSBundle:Page')->getCachedPagesDataIndexedById();

        if (isset($pages[$id])) {
            return $this->router->generate('page', array(
                'slug' => $pages[$id]['slug'],
            ));
        }

        return '#';
    }

    /**
     * Generate path to a Menu.
     *
     * @param integer $id
     *
     * @return string
     */
    public function getMenuPath($id)
    {
        $menus = $this->em->getRepository('LexikCMSBundle:Menu')->getCachedMenuByUnitIndexedById();

        if (!isset($menus[$id])) {
            return '#';
        }

        $menu = $menus[$id];

        if (null !== $menu['page']) {
            return $this->getPagePath($menu['page']);
        }

        if (null !== $menu['route']) {
            return $this->router->generate($menu['route']);
        }

        return $menu['url'];
    }

    /**
     * Retrieve the block content by his reference.
     *
     * @param string  $reference
     * @param boolean $cache
     *
     * @return string
     */
    public function getBlockContent($reference, $cache = true)
    {
        $blocks = $this->em->getRepository('LexikCMSBundle:Block')->getCachedBlocks($cache);

        return (is_array($blocks) && isset($blocks[$reference])) ? $blocks[$reference] : null;
    }

    /**
     * Retrieve the block content by his reference.
     *
     * @param string $slug
     *
     * @return \Lexik\Bundle\CMSBundle\Entity\Page|null
     */
    public function getPageContent($slug)
    {
        return $this->em->getRepository('LexikCMSBundle:Page')->getCachedPage($slug);
    }
}
