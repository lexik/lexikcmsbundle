<?php

namespace Lexik\Bundle\CMSBundle\Service;

use Doctrine\ORM\EntityManager;
use Lexik\Bundle\CMSBundle\Entity\Page;
use Symfony\Component\Routing\RouterInterface;

/**
 * PageTransformer
 */
class PageTransformer
{
    /**
     * @var RouterInterface $router
     */
    private $router;

    /**
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * Transforms a page.
     *
     * @param Page          $page
     * @param EntityManager $em
     */
    public function transform(Page $page, EntityManager $em)
    {
        $cmsRouter = new Router($this->router, $em);

        // transform content
        $content = $page->getContent();
        $content = $this->transformPageLinks($content, $cmsRouter);
        $page->setTransformedContent($content);
    }

    /**
     * Applies page links transformations.
     *
     * @param string $content
     * @param Router $cmsRouter
     *
     * @return string
     */
    protected function transformPageLinks($content, Router $cmsRouter)
    {
        $content = preg_replace_callback(
            '`#page:([^#]+)#`',
            function ($matches) use ($cmsRouter) {
                return $cmsRouter->getPagePath($matches[1]);
            },
            $content
        );

        $content = preg_replace_callback(
            '`#menu:([^#]+)#`',
            function ($matches) use ($cmsRouter) {
                return $cmsRouter->getFilePath($matches[1]);
            },
            $content
        );

        return $content;
    }

    /**
     * Applies spaceless transformation.
     *
     * @param string $content
     *
     * @return string
     */
    protected function transformSpaceless($content)
    {
        $search = array(
            '/>[^\S ]+/s', // strip whitespaces after tags, except space
            '/[^\S ]+</s', // strip whitespaces before tags, except space
            '/(\s)+/s'      // shorten multiple whitespace sequences
        );
        $replace = array(
            '>',
            '<',
            '\\1'
        );

        return preg_replace($search, $replace, $content);
    }
}
