<?php

namespace Lexik\Bundle\CMSBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Repository for Block entity
 */
class BlockRepository extends EntityRepository
{
    /**
     * Returns cached blocks.
     *
     * @param bool $cache
     *
     * @return array
     */
    public function getCachedBlocks($cache = true)
    {
        $dql = <<<DQL
SELECT b.id, b.name, b.content, b.lastModified
FROM Lexik\Bundle\CMSBundle\Entity\Block b INDEX BY b.name
DQL;

        $query = $this
            ->getEntityManager()
            ->createQuery($dql);

        if ($cache) {
            $query->useResultCache(true, 3600 * 24 * 30, 'blocks');
        }

        return $query->getArrayResult();
    }
}
