<?php

namespace Lexik\Bundle\CMSBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

/**
 * Class BlockRouteRepository
 *
 * @package Lexik\Bundle\CMSBundle\Repository
 */
class BlockRouteRepository extends EntityRepository
{
    /**
     * Searchs blocks that contains the query string.
     *
     * @param string $query
     *
     * @return array
     */
    public function search($query)
    {
        return $this
            ->createQueryBuilder('br')
            ->select('br.route, br.title AS label')
            ->innerJoin('br.blocks', 'b', Expr\Join::WITH, 'b.content LIKE :query AND b.isSearchable = :searchable')
            ->setParameters(array(
                'query'      => sprintf('%%%s%%', $query),
                'searchable' => true,
            ))
            ->getQuery()
            ->getResult()
        ;
    }
}
