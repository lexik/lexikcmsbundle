<?php

namespace Lexik\Bundle\CMSBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * Repository for Menu entity
 */
class MenuRepository extends EntityRepository
{
    /**
     * Returns data about menu items of a given unit.
     *
     * @param string $unit
     *
     * @return array
     */
    public function getCachedMenuByUnitIndexedById($unit = null)
    {
        $qb = $this
            ->getEntityManager()
            ->createQueryBuilder()
            ->select('m.id, m.label, p.id AS page, p.slug AS page_slug, m.route, m.url, m.unit, m.sequence')
            ->from($this->getEntityName(), 'm', 'm.id')
            ->leftJoin('m.page', 'p')
            ->orderBy('m.sequence');

        if (null !== $unit) {
            $qb
                ->where('m.unit = :unit')
                ->setParameter('unit', $unit);
        }

        $query = $qb->getQuery();
        $cacheKey = null !== $unit ? sprintf('menu_%s', $unit) : 'menus';
        $query->useResultCache(true, 3600 * 24 * 30, $cacheKey);

        return $query->getArrayResult();
    }
}
