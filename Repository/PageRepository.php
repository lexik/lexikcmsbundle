<?php

namespace Lexik\Bundle\CMSBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query;
use Doctrine\DBAL\Types\Type;

/**
 * Repository for Page entity
 */
class PageRepository extends EntityRepository
{
    /**
     * Returns some data about pages indexed by page ID.
     *
     * @return array
     */
    public function getCachedPagesDataIndexedById()
    {
        return $this
            ->getEntityManager()
            ->createQueryBuilder()
            ->select('a.id, a.slug')
            ->from($this->getEntityName(), 'a', 'a.id')
            ->getQuery()
            ->useResultCache(true, 3600 * 24 * 30, 'pages')
            ->getArrayResult();
    }

    /**
     * Returns a cached
     *
     * @param $slug
     *
     * @return \Lexik\Bundle\CMSBundle\Entity\Page|null
     */
    public function getCachedPage($slug)
    {
        $dql = <<<DQL
SELECT p.id, p.slug, p.metaTitle, p.metaDescription, p.transformedContent
FROM Lexik\Bundle\CMSBundle\Entity\Page p
WHERE p.slug = :slug
DQL;

        return $this
            ->getEntityManager()
            ->createQuery($dql)
            ->setParameter('slug', $slug, Type::STRING)
            ->useResultCache(true, 3600 * 24 * 30, sprintf('page_%s', $slug))
            ->getOneOrNullResult(Query::HYDRATE_ARRAY);
    }

    /**
     * Returns a cached
     *
     * @return \Lexik\Bundle\CMSBundle\Entity\Page|null
     */
    public function getCachedHomepage()
    {
        $dql = <<<DQL
SELECT p.id, p.slug, p.metaTitle, p.metaDescription, p.transformedContent
FROM Lexik\Bundle\CMSBundle\Entity\Page p
WHERE p.slug IS NULL
DQL;

        try {
            return $this
                ->getEntityManager()
                ->createQuery($dql)
                ->setMaxResults(1)
                ->useResultCache(true, 3600 * 24 * 30, 'homepage')
                ->getSingleResult(Query::HYDRATE_ARRAY);
        } catch (NoResultException $e) {
            return;
        }
    }

    /**
     * @param string $query
     *
     * @return array
     */
    public function search($query)
    {
        return $this
            ->createQueryBuilder('p')
            ->select('p.slug, p.pageTitle AS label')
            ->where('p.content LIKE :query')
            ->orWhere('p.pageTitle LIKE :query')
            ->setParameters(array(
                'query' => sprintf('%%%s%%', $query),
            ))
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY)
        ;
    }
}
