<?php

namespace Lexik\Bundle\CMSBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * MenuType
 */
class MenuType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('label')
            ->add('unit')
            ->add('sequence')
            ->add('page', 'entity', array(
                'class'       => 'LexikCMSBundle:Page',
                'empty_value' => '',
                'required'    => false,
            ))
            ->add('route', null, array('required' => false))
            ->add('url', null, array('required' => false))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Lexik\Bundle\CMSBundle\Entity\Menu'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'lexik_cms_menu';
    }
}
