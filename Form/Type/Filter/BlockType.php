<?php

namespace Lexik\Bundle\CMSBundle\Form\Type\Filter;

use Lexik\Bundle\FormFilterBundle\Filter\FilterOperands;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * BlockType
 */
class BlockType extends AbstractType
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                'filter_text',
                array(
                    'condition_pattern' => FilterOperands::STRING_BOTH
                )
            )
        ;
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'lexik_cms_block';
    }
}
